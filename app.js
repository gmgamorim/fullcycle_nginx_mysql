'use strict';

const express = require('express');

async function connect(){
  if(global.connection && global.connection.state !== 'disconnected')
      return global.connection;

  const mysql = require("mysql2/promise");
  const connection = await mysql.createConnection("mysql://root:mysecretpassword@db:3306/mydb");
  console.log("Conectou no MySQL!");
  global.connection = connection;
  return connection;
}

// Constants
const PORT = 3000;
// App
const app = express();
app.get('/', async (req, res) => {
  var connection = await connect();
  const [rows] = await connection.query('SELECT name FROM people LIMIT 1');
  console.log(rows);

  let response = `<h1>Full Cycle Rocks!</h1><br /><h2>${rows[0].name}</h2>`;
  res.send(response);
});


async function setupDb () {
  // Delays the connections to allow the db to be up
  var connection = await connect();

  try {
    var createTableQuery = "CREATE TABLE people (name VARCHAR(255))";
    await connection.query(createTableQuery);
    console.log("Table created")
  } catch {
    console.log("Table already exists")
  }
  
  try {
    var insertDataQuery = "INSERT INTO people (name) VALUES ('Guilherme')";
    await connection.query(insertDataQuery);
    console.log("1 record inserted");
  } catch {
    console.log("record already exists")
  }
}

app.listen(PORT, async function(err) {
  if (err) console.log("Error in server setup");
  
  setupDb();

  console.log("Server listening on Port", PORT);
});